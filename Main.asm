;=============================================================================
; ASM - STL
;
; Main program file for the STL 3D-model viewer, written in x86 32-bit Assembler
;=============================================================================
IDEAL
P386
MODEL FLAT, C
ASSUME cs:_TEXT,ds:FLAT,es:FLAT,fs:FLAT,gs:FLAT

INCLUDE "VGA.INC"
INCLUDE "DRAWING.INC"
INCLUDE "IMPORT.INC"
INCLUDE "MATH.INC"
INCLUDE "MOUSE.INC"

;=============================================================================
; CODE
;=============================================================================
CODESEG

;+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
; Exits the program and goes back to the DOS command prompt
;
; ARGUMENTS:
;   None
; RETURNS:
;   None
;+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
PROC exit
	USES eax

	call mouse_uninstall
	call vga_unsetupMode
	mov	eax, 4c00h
	int 21h

	ret
ENDP exit

;+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
; Initializing the program
;
; ARGUMENTS:
;   None
; RETURNS:
;   None
;+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
PROC initProgram
	USES eax
	
	call vga_setupMode
	call openFile

	call mouse_present
    cmp eax, 1
    jne @@skipMouse
	
		call mouse_install, offset mouse_handler
		
	@@skipMouse:
	ret
ENDP initProgram

;+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
; Handle all user inputs (keyboard and mouse)
;
; ARGUMENTS:
;   None
; RETURNS:
;   None
;+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
PROC handleUserInput
	USES eax, ebx, ecx

	mov ah, 01h ; function 01h (test key pressed)
	int 16h		; call keyboard BIOS
	jz @@no_key_pressed
	mov ah, 00h
	int 16h

	; process key code here (scancode in AH, ascii code in AL)
	cmp ah, 01	; scancode for ESCAPE key
	jne	@@n1
		call exit
		jmp @@no_key_pressed

	@@n1:
		cmp ah, 77	; arrow right
		jne @@n2
		;call moveBlock, 1
		jmp @@no_key_pressed

	@@n2:
		cmp ah, 75	; arrow left
		jne @@n3
		;call moveBlock, -1
		jmp @@no_key_pressed

	@@n3:
		cmp ah, 80	; arrow down
		jne @@n4
		;call moveBlock, 10
		jmp @@no_key_pressed

	@@n4:
		cmp ah, 72	; arrow up
		jne @@n5
		;call rotateBlock
		jmp @@no_key_pressed

	@@n5:
	@@no_key_pressed:

	call mouse_calculateRotationAndZoom

	call fixedDivision, eax, [ROTATION_PRECISION]
	cmp eax, 0
	je @@bNotChanged
		mov [viewChanged], 1
		call fixedAddition, [bRotation], eax
		call getNormalizedAngle, eax
		mov [bRotation], eax
	@@bNotChanged:
	
	call fixedDivision, ebx, [ROTATION_PRECISION]
	cmp eax, 0
	je @@aNotChanged
		mov [viewChanged], 1
		call fixedAddition, [aRotation], eax
		call getNormalizedAngle, eax
		mov [aRotation], eax
	@@aNotChanged:	
	
	call fixedDivision, ecx, [ZOOM_PRECISION]
	cmp eax, 0
	je @@zoomNotChanged
		mov [viewChanged], 1
		call fixedAddition, [zoom], eax
		call fixedMax, eax, 0
		mov [zoom], eax
	@@zoomNotChanged:	
	
	ret
ENDP handleUserInput

;+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
; Draw everything!
;
; ARGUMENTS:
;   None
; RETURNS:
;   None
;+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
PROC drawAll
	call vga_waitVBE
	call draw_clear
	
	; Draw file name
	call draw_text, offset fileNameTxt, 0, 0
	call getFileName
	call draw_text, eax, 11, 0
	
	; Draw parameters
	call draw_text, offset zoomTxt, 0, 24
	call setCursorPosition, 6, 24
	call printFixed, [zoom]
		
	call draw_object, [aRotation], [bRotation], [zoom]
	ret
ENDP drawAll

; MAIN Start of program
start:
    sti                             ; Set The Interrupt Flag
    cld                             ; Clear The Direction Flag

    push ds 						; Put value of DS register on the stack
    pop es 							; And write this value to ES

	; Set-up and initialization
	call initProgram

	; Main program (endless) loop
	call drawAll
	@@programLoop:
		call handleUserInput
		cmp [viewChanged], 0
		je @@skipDrawing
			call drawAll
			mov [viewChanged], 0
		@@skipDrawing:
		jmp @@programLoop

	mov	eax, 4c00h
	int 21h


;=============================================================================
; DATA
;=============================================================================
DATASEG
	aRotation				dd 	0
	bRotation				dd 	0
	zoom					dd 	40000h
	viewChanged				db 	0

	ROTATION_PRECISION		dd 	640000h
	ZOOM_PRECISION			dd 	80000h
	
	fileNameTxt				db  "File name:", "$"
	zoomTxt					db  "Zoom:", "$"
	
;=============================================================================
; STACK
;=============================================================================
STACK 1000h

END start
