import math

def getFixedPointNr(value):
    return int(round(value * math.pow(2, 16)))

def getHex(value):
    if value < 0:
        value = abs(value) | (1 << 31)
    s = (hex(value)[2:]).upper().strip('L') + 'h'
    if not s[0] in "0123456789":
        s = '0' + s
    return s


tempStr = ""
for i in range(0, int(math.pow(2, 10))):
    if i%8==0 and i != 0:
        print tempStr + " \\"
        tempStr = ""
    tempStr = tempStr + getHex(getFixedPointNr(math.sin(2*math.pi*float(i)/int(math.pow(2, 10))))) + ", "

print tempStr + " \\"
